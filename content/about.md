+++
author = ["Fermin MF"]
draft = false
+++

## About {#about}

My name is Fermin, i'm a programmer and Emacs fanatic.

-   Some of my interests are:
    -   Thinkpads (Good old thinkpads)
    -   Lisp dialects (Scheme,Clojure and Elisp)
    -   Web development (PHP [Drupal and Codeigniter])
    -   Emacs (In general, Emacs is great)
    -   Productivity tools (IDE features and programming tools)

-   Email: fmfs@posteo.net
