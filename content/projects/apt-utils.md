+++
author = ["Fermin MF"]
lastmod = 2020-05-03T13:14:49+02:00
draft = false

title = "Apt-utils emacs package"
tags = [
    "debian",
    "apt",
    "packages",
	"dired"
]
categories = [
    "emacs",
    "debian",
]
+++

## Apt-utils emacs package {#apt-utils-emacs-package}

#### Emacs package for apt debian package management {#emacs-package-for-apt-debian-package-management}

This is a small project, focus on the maintenance of the code, and optimization of the legacy code some of the features are:

-   Output from the apt-cache program is presented.
-   Related packages are accessible via hyperlinks.
-   Perform package searches using regular expressions, and more sophisticated searches using grep-dctrl.
-   Easy exploration of packages and their normal and reverse dependencies.
-   Quick access to package ChangeLog, README, NEWS and copyright files.
-   Quick access to package man pages.
-   Browse online ChangeLog and copyright files, bug reports and package versions.
-   List files in installed packages (using Dired).

<!--list-separator-->
More information [apt-utils](https://gitlab.com/sasanidas/apt-utils)
