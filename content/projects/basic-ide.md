+++
author = ["Fermin MF"]
lastmod = 2020-05-09T19:07:27+02:00
draft = false
title = "basic-ide emacs package"
tags = [
    "packages",
	"c64",
	"retro",
	"IDE"
]
categories = [
    "emacs",
    "development",
]

+++

## basic-ide emacs package {#basic-ide-emacs-package}

#### Emacs package for c64 basic development {#emacs-package-for-c64-basic-development}

This is a minor mode for emacs, that provide some IDE features for the basic-mode , it has flycheck,company and helm support,
it also provide an easy way to integrate Simon's BASIC.

Provide 3 interpreters cbmbasic,Bas 2.5 and of course VICE.

For the moment, it's focus on the c64 support.

<!--list-separator-->


<!--list-separator-->
More information [basic-ide](https://gitlab.com/sasanidas/emacs-c64-basic-ide)
