#+hugo_base_dir: ../

#+hugo_weight: auto
#+hugo_auto_set_lastmod: t


#+author: Fermin MF

* basic-ide emacs package                                            :@emacs:
** TODO Writing Hugo blog in Org                                   :hugo:org:
:PROPERTIES:
:EXPORT_FILE_NAME: writing-hugo-blog-in-org-subtree-export
:EXPORT_DATE: DATE
:EXPORT_HUGO_MENU: :menu "main"
:EXPORT_HUGO_CUSTOM_FRONT_MATTER: :foo bar :baz zoo :alpha 1 :beta "two words" :gamma 10
:END:
*** Emacs package for c64 basic development 
This is a minor mode for emacs, that provide some IDE features for the basic-mode , it has flycheck,company and helm support,
it also provide an easy way to integrate Simon's BASIC.
Provide 3 interpreters cbmbasic,Bas 2.5 and of course VICE.
For the moment, it's focus on the c64 support.

**** A sub-heading under that heading
- It's draft state will be marked as =true= as the subtree has the
  todo state set to /TODO/.

With the point _anywhere_ in this /Writing Hugo blog in Org/ post
subtree, do =C-c C-e H H= to export just this post.

The exported Markdown has a little comment footer as set in the /Local
Variables/ section below.
* Footnotes
* COMMENT Local Variables                                           :ARCHIVE:
# Local Variables:
# org-hugo-footer: "\n\n[//]: # \"Exported with love from a post written in Org mode\"\n[//]: # \"- https://github.com/kaushalmodi/ox-hugo\""
# End:
